
DROP TABLE IF EXISTS `configs`;
CREATE TABLE IF NOT EXISTS `configs`(
    `config`   INT UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
    `md5_hash` BINARY(16),
    `data`     LONGBLOB,
    CONSTRAINT UNIQUE(`md5_hash`)
);


INSERT INTO `configs` (`md5_hash`, `data`) VALUES (X'6666efdfb4499fad5e9eefdfb4499999', X'234523452345ABABABABBABABABB');
INSERT INTO `configs` (`md5_hash`, `data`) VALUES (X'777fad5e9eefdfb4509fad5e9eefdfb4', X'234523452345ABAFBFBFBFBFBFBFBBABABAB');

SELECT * FROM `configs`;



DELIMITER //
DROP PROCEDURE IF EXISTS get_config_by_hash//
CREATE PROCEDURE get_config_by_hash (IN hash BINARY(16))
     BEGIN
        SELECT * FROM `configs` WHERE `configs`.`md5_hash` = hash
        LIMIT 1;        
     END
//
DELIMITER ;


DELIMITER //
DROP PROCEDURE IF EXISTS get_config_by_n//
CREATE PROCEDURE get_config_by_n (IN num INT UNSIGNED)
     BEGIN
        SELECT * FROM `configs` WHERE `configs`.`config` = num
        LIMIT 1;        
     END
//
DELIMITER ;





CREATE TABLE IF NOT EXISTS `event_settings`(
    `config`   INT UNSIGNED,
    `code`     SMALLINT UNSIGNED,
    `edge`     TINYINT UNSIGNED,
    `text`     VARCHAR(65500),
    `type`     TINYINT UNSIGNED,
    `color`    INT UNSIGNED,
    CONSTRAINT PRIMARY KEY USING BTREE (`config`, `code`, `edge`)
);




INSERT INTO `event_settings` ( `config`, `code`, `edge`, `text`, `type`, `color`) VALUES (1, 0, 0, '001sdvdfsdfv', 1, 65535);
INSERT INTO `event_settings` ( `config`, `code`, `edge`, `text`, `type`, `color`) VALUES (1, 0, 1, '011sdvdfsdfv', 1, 6535);
INSERT INTO `event_settings` ( `config`, `code`, `edge`, `text`, `type`, `color`) VALUES (1, 1, 0, '101sdvdfsdfv', 1, 10000);
INSERT INTO `event_settings` ( `config`, `code`, `edge`, `text`, `type`, `color`) VALUES (1, 1, 1, '111sdvdfsdfv', 1, 12000);
INSERT INTO `event_settings` ( `config`, `code`, `edge`, `text`, `type`, `color`) VALUES (1, 2, 0, '201sdvdfsdfv', 1, 444444);
INSERT INTO `event_settings` ( `config`, `code`, `edge`, `text`, `type`, `color`) VALUES (1, 2, 1, '211sdvdfsdfv', 1, 222222);
INSERT INTO `event_settings` ( `config`, `code`, `edge`, `text`, `type`, `color`) VALUES (2, 0, 0, '002sdvdfsdfv', 1, 65535);
INSERT INTO `event_settings` ( `config`, `code`, `edge`, `text`, `type`, `color`) VALUES (2, 0, 1, '012sdvdfsdfv', 1, 6535);
INSERT INTO `event_settings` ( `config`, `code`, `edge`, `text`, `type`, `color`) VALUES (2, 1, 0, '102sdvdfsdfv', 1, 10000);
INSERT INTO `event_settings` ( `config`, `code`, `edge`, `text`, `type`, `color`) VALUES (2, 1, 1, '112sdvdfsdfv', 1, 12000);
INSERT INTO `event_settings` ( `config`, `code`, `edge`, `text`, `type`, `color`) VALUES (2, 2, 0, '202sdvdfsdfv', 1, 444444);
INSERT INTO `event_settings` ( `config`, `code`, `edge`, `text`, `type`, `color`) VALUES (2, 2, 1, '212sdvdfsdfv', 1, 222222);

SELECT * FROM `event_settings`;






CREATE TABLE IF NOT EXISTS `event_headers`(
    `id`       BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `surza_id` TINYINT UNSIGNED,
    `event_id` BIGINT UNSIGNED,
    `time`     DATETIME(6),
    `unixtime` BIGINT,
    `nanosec`  INT UNSIGNED,   
    `steady_nanosec` BIGINT UNSIGNED,
    `config`   INT UNSIGNED,
    CONSTRAINT UNIQUE(`surza_id`, `event_id`),
    CONSTRAINT FOREIGN KEY (`config`) REFERENCES `configs` (`config`)
    ON DELETE SET NULL
    ON UPDATE SET NULL
);






CREATE TABLE IF NOT EXISTS `events`(    
    `n`        BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `code`     SMALLINT UNSIGNED,
    `edge`     TINYINT UNSIGNED,
    `id`       BIGINT UNSIGNED,
    FOREIGN KEY (`id`) REFERENCES `event_headers` (`id`)
    ON DELETE SET NULL
    ON UPDATE SET NULL
);



DELIMITER //
DROP PROCEDURE IF EXISTS get_last_event_n//
CREATE PROCEDURE get_last_event_n ()
     BEGIN
        SELECT `n` FROM `events`
        ORDER BY `n` DESC
        LIMIT 1;
     END
//
DELIMITER ;




INSERT INTO `event_headers` (`surza_id`, `event_id`, `time`, `unixtime`, `nanosec`, `steady_nanosec`, `config`)
VALUES (0, 1, '2013-01-18 13:44:22.123456', 56434564553, 3000000, 234523452345, 1);
INSERT INTO `events` (`code`, `edge`, `id`)
VALUES (0, 1, 1);
INSERT INTO `events` (`code`, `edge`, `id`)
VALUES (1, 1, 1);
INSERT INTO `events` (`code`, `edge`, `id`)
VALUES (2, 1, 1);


INSERT INTO `event_headers` (`surza_id`, `event_id`, `time`, `unixtime`, `nanosec`, `steady_nanosec`, `config`)
VALUES (0, 2, '2012-02-18 13:45:23.333333', 56434564555, 100000, 244523452345, 1);
INSERT INTO `events` (`code`, `edge`, `id`)
VALUES (0, 0, 2);
INSERT INTO `events` (`code`, `edge`, `id`)
VALUES (1, 0, 2);
INSERT INTO `events` (`code`, `edge`, `id`)
VALUES (2, 0, 2);


INSERT INTO `event_headers` (`surza_id`, `event_id`, `time`, `unixtime`, `nanosec`, `steady_nanosec`, `config`)
VALUES (0, 3, '2013-01-18 13:46:22.123456', 56434564556, 4000000, 254523452345, 1);
INSERT INTO `events` (`code`, `edge`, `id`)
VALUES (1, 0, 3);
INSERT INTO `events` (`code`, `edge`, `id`)
VALUES (2, 0, 3);


INSERT INTO `event_headers` (`surza_id`, `event_id`, `time`, `unixtime`, `nanosec`, `steady_nanosec`, `config`)
VALUES (0, 4, '2013-01-18 13:47:22.123456', 56434564557, 5000000, 264523452345, 2);
INSERT INTO `events` (`code`, `edge`, `id`)
VALUES (0, 1, 4);


INSERT INTO `event_headers` (`surza_id`, `event_id`, `time`, `unixtime`, `nanosec`, `steady_nanosec`, `config`)
VALUES (0, 5, '2013-01-18 13:48:22.123456', 56434564558, 6000000, 274523452345, 2);
INSERT INTO `events` (`code`, `edge`, `id`)
VALUES (0, 0, 5);
INSERT INTO `events` (`code`, `edge`, `id`)
VALUES (1, 0, 5);
INSERT INTO `events` (`code`, `edge`, `id`)
VALUES (2, 0, 5);



SELECT * FROM `events`;




DELIMITER //
DROP PROCEDURE IF EXISTS get_events_after//
CREATE PROCEDURE get_events_after (IN ev_n BIGINT UNSIGNED, IN lim SMALLINT UNSIGNED)
     BEGIN
        SELECT `events`.`n`,
               `event_headers`.`surza_id`,
               `event_headers`.`event_id`,
               `event_headers`.`time`,
               `event_headers`.`unixtime`,
               `event_headers`.`nanosec`,
               `event_headers`.`steady_nanosec`,
               `events`.`code`,
               `events`.`edge`,
               `event_settings`.`text`
        FROM `events`
        LEFT JOIN `event_headers` ON (`events`.`id` = `event_headers`.`id`)
        LEFT JOIN `event_settings` ON (`event_headers`.`config` = `event_settings`.`config` AND `events`.`code` = `event_settings`.`code` AND `events`.`edge` = `event_settings`.`edge` )
        WHERE `events`.`n` > ev_n LIMIT lim;       
     END
//
DELIMITER ;




DELIMITER //
DROP PROCEDURE IF EXISTS get_last_events//
CREATE PROCEDURE get_last_events (IN num SMALLINT UNSIGNED)
     BEGIN
        DECLARE last BIGINT UNSIGNED DEFAULT 0;
        SELECT ifnull(max(`n`),0) INTO last FROM `events`;
        call get_events_after( if(last > num, last - num, 0), num );
     END
//
DELIMITER ;









CREATE TABLE IF NOT EXISTS `event_data`(
    `id`     BIGINT UNSIGNED,
    `config` INT UNSIGNED,
    `data`   BLOB,
    CONSTRAINT UNIQUE (`id`),
    FOREIGN KEY (`id`) REFERENCES `event_headers` (`id`)
    ON DELETE SET NULL
    ON UPDATE SET NULL,
    FOREIGN KEY (`config`) REFERENCES `configs` (`config`)
    ON DELETE SET NULL
    ON UPDATE SET NULL
);

INSERT INTO `event_data` (`id`, `config`, `data`) VALUES (1, 1, X'ABABABA123123123123543452345');
INSERT INTO `event_data` (`id`, `config`, `data`) VALUES (2, 1, X'FFABABA123123123123543452345');
INSERT INTO `event_data` (`id`, `config`, `data`) VALUES (3, 1, X'BBABABA123123123123543452345');
INSERT INTO `event_data` (`id`, `config`, `data`) VALUES (4, 2, X'CCABABA123123123123543452345');
INSERT INTO `event_data` (`id`, `config`, `data`) VALUES (5, 2, X'DDABABA123123123123543452345');

SELECT * FROM `event_data`;




DELIMITER //
DROP PROCEDURE IF EXISTS get_event_data//
CREATE PROCEDURE get_event_data (IN id_p BIGINT UNSIGNED)
     BEGIN
        SELECT * FROM `event_data`
        WHERE (`id` = id_p)
        LIMIT 1;        
     END
//
DELIMITER ;





CREATE TABLE IF NOT EXISTS `event_data_settings_qty`(
    `config` INT UNSIGNED,
    `f_num`  SMALLINT UNSIGNED,
    `i_num`  SMALLINT UNSIGNED,
    `b_num`  SMALLINT UNSIGNED,
    CONSTRAINT UNIQUE (`config`),
    FOREIGN KEY (`config`) REFERENCES `configs` (`config`)
    ON DELETE SET NULL
    ON UPDATE SET NULL
);

INSERT INTO `event_data_settings_qty` (`config`, `f_num`, `i_num`, `b_num`) VALUES (1, 5, 3, 10);
INSERT INTO `event_data_settings_qty` (`config`, `f_num`, `i_num`, `b_num`) VALUES (2, 4, 8, 16);

SELECT * FROM `event_data_settings_qty`;



DELIMITER //
DROP PROCEDURE IF EXISTS get_event_settings_qty//
CREATE PROCEDURE get_event_settings_qty (IN conf_p INT UNSIGNED)
     BEGIN
        SELECT `f_num`, `i_num`, `b_num` FROM `event_data_settings_qty`
        WHERE (`config` = conf_p)
        LIMIT 1;        
     END
//
DELIMITER ;





CREATE TABLE IF NOT EXISTS `event_data_settings_text`(
    `config` INT UNSIGNED,
    `type`   TINYINT UNSIGNED,
    `index`  SMALLINT UNSIGNED,
    `text`   VARCHAR(65000),
    `unit`   VARCHAR(255),
    `color`  INT UNSIGNED,
    FOREIGN KEY (`config`) REFERENCES `configs` (`config`)
    ON DELETE SET NULL
    ON UPDATE SET NULL,
    CONSTRAINT UNIQUE (`config`, `type`, `index`)
);

INSERT INTO `event_data_settings_text` (`config`, `type`, `index`, `text`, `unit`, `color`) VALUES (1, 0, 0, '1dfvfdvasdfvsdf', 'kA', 5342234);
INSERT INTO `event_data_settings_text` (`config`, `type`, `index`, `text`, `unit`, `color`) VALUES (1, 0, 1, '2dfvfdvasdfvsdf', 'kV', 53434);
INSERT INTO `event_data_settings_text` (`config`, `type`, `index`, `text`, `unit`, `color`) VALUES (1, 0, 2, '3dfvfdvasdfvsdf', 'kV', 533334);
INSERT INTO `event_data_settings_text` (`config`, `type`, `index`, `text`, `unit`, `color`) VALUES (1, 1, 0, '4dfvfdvasdfvsdf', 'V', 53434);
INSERT INTO `event_data_settings_text` (`config`, `type`, `index`, `text`, `unit`, `color`) VALUES (1, 1, 2, '5dfvfdvasdfvsdf', 'A', 3332234);
INSERT INTO `event_data_settings_text` (`config`, `type`, `index`, `text`, `unit`, `color`) VALUES (1, 2, 0, '5dfvfdvasdfvsdf', 'A', 666234);
INSERT INTO `event_data_settings_text` (`config`, `type`, `index`, `text`, `unit`, `color`) VALUES (2, 0, 0, '1sdfsdsdxxxxxxx', 'kA', 5342234);
INSERT INTO `event_data_settings_text` (`config`, `type`, `index`, `text`, `unit`, `color`) VALUES (2, 0, 1, '2sdfsdsdxxxxxxx', 'kV', 53434);
INSERT INTO `event_data_settings_text` (`config`, `type`, `index`, `text`, `unit`, `color`) VALUES (2, 0, 2, '3sdfsdsdxxxxxxx', 'kV', 533334);
INSERT INTO `event_data_settings_text` (`config`, `type`, `index`, `text`, `unit`, `color`) VALUES (2, 1, 0, '4sdfsdsdxxxxxxx', 'V', 53434);
INSERT INTO `event_data_settings_text` (`config`, `type`, `index`, `text`, `unit`, `color`) VALUES (2, 1, 2, '5sdfsdsdxxxxxxx', 'A', 3332234);
INSERT INTO `event_data_settings_text` (`config`, `type`, `index`, `text`, `unit`, `color`) VALUES (2, 2, 0, '5sdfsdsdxxxxxxx', 'A', 666234);

SELECT * FROM `event_data_settings_text`;



DELIMITER //
DROP PROCEDURE IF EXISTS get_event_settings_text//
CREATE PROCEDURE get_event_settings_text (IN conf_p INT UNSIGNED)
     BEGIN
        SELECT `type`, `index`, `text`, `unit`, `color` FROM `event_data_settings_text`
        WHERE (`config` = conf_p);
     END
//
DELIMITER ;

